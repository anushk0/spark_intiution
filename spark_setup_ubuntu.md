# PySpark-3.3.2 installation on Ubuntu-22.04

1. Open [spark download page]("https://spark.apache.org/downloads.html")

2. After chosing spark release and package type, Download spark from hyperlink on package name or [Direct Download click here](https://www.apache.org/dyn/closer.lua/spark/spark-3.3.2/spark-3.3.2-bin-hadoop3.tgz)

3. open terminal at downloads 
	```bash
	tar xvzf spark-3.3.2-bin-hadoop3.tgz
	```

4. Create "usr/local/spark" directory
    ```bash
	sudo mkdir -p /usr/local/spark
	```

5. Goto "usr/local/spark" 
   ```bash
	dir cd usr/local/spark
   ```

6. Move the extracted downloaded folder to "usr/local/spark" dir
   	```bash
	sudo mv ~/Downloads/spark-3.3.2-bin-hadoop3 /usr/local/spark
	```

7. Check whether move was successful
	```bash
	ls -al
	```

8.  Check where python is present
	```bash
	whereis python
	```

9.  choose the path to conda python else default python path i.e. usr/bin/python3

10. Open ".bashrc" file at "home" directory and append the below text:
```m
# >>> PySpark setup >>>
export SPARK_HOME="/usr/local/spark/spark-3.3.2-bin-hadoop3"
export PATH="${PATH}:${SPARK_HOME}/bin"

export PYSPARK_PYTHON="/home/aimlytics/anaconda3/bin/python3" # path from step 9 to be followed
export PYSPARK_DRIVER_PYTHON="/home/aimlytics/anaconda3/bin/python3" # ath from step 9 to be followed
# <<< PySpark Setup <<<
```

11. After saving the above changes, execute the file
    ```bash
	source ~/.bashrc
	```
	* check SPARK Home
		```bash
		echo $SPARK_HOME
		```

12. install pip
    ```bash
	sudo apt install python3-pip -y
	```

13. install pyspark using pip3
	```bash
	sudo pip3 install pyspark
	```
14. Check if pyspark is installed
    ```bash
	sudo pip3 freeze
	```

15. Goto "/usr/local/spark/spark-3.3.2-bin-hadoop3/bin"
    ```bash
	cd $SPARK_HOME/bin
	```

16. Try Opening spark shell
    ```bash
	./spark-shell JAVA_HOME
	```

17. If Java is not pre-installed; to install JAVA execute:
    ```bash
	cd
	sudo apt install openjdk-8-jre
	sudo apt install openjdk-8-jdk
	java -version
	```

18. Goto "/usr/local/spark/spark-3.3.2-bin-hadoop3/bin"
    ```bash
	cd $SPARK_HOME/bin
	```

19. Open spark shell
    ```bash
	./spark-shell
	``` 
	* Close spark shell
		```scala
		:q
		```

20. Open pyspark shell
    ```bash
	./pyspark
	```
	* Close pyspark shell
		```python
		exit()
		```

**Follow Up**:
> [Anaconda and Apache Spark 3.0 — Easy install in Ubuntu 20.04](https://medium.com/@miguel.brito/anaconda-and-apache-spark-3-0-easy-install-in-ubuntu-20-04-a66a6d792e7a)